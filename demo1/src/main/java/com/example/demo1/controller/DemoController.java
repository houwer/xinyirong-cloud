package com.example.demo1.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/testcontroller")
@Api(value = "DemoController 测试")
public class DemoController {

    @RequestMapping(value = "/getone", method = RequestMethod.POST)
    @ApiOperation(value = "demo演示", notes = "demo 模块演示接口", httpMethod = "POST")
    @GetMapping
    public String getTwo() {
        return "getone demo 模块演示接口 ";
    }

    @RequestMapping(value = "/gettwo", method = RequestMethod.GET)
    @ApiOperation(value = "demo演示", notes = "demo 模块演示接口", httpMethod = "GET")
    public String getOne() {
        return "gettwo demo 模块演示接口 ";
    }
}
