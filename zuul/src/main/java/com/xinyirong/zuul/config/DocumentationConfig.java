/**
 * @program: xinyirong-cloud
 * @author: Ding Houwen
 * @create: 2020-02-12 16:50
 **/

package com.xinyirong.zuul.config;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

@Component
@Primary
public class DocumentationConfig implements SwaggerResourcesProvider {

    @Override
    public List<SwaggerResource> get() {
        List resources = new ArrayList<>();
        resources.add(swaggerResource("demo1-test", "/demo1/v2/api-docs?group=demo1-test", "1.0"));
        resources.add(swaggerResource("demo2-test", "/demo2/v2/api-docs?group=demo2-test", "1.0"));
        //resources.add(swaggerResource("检查系统接口", "/study-proxy/prevention-check/v2/api-docs", "1.0"));
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}