package com.xinyirong.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "json 接口实体")
public class Result<T>{

    private static final int CODE_SUCCESS = 200;
    private static final int CODE_FAIL = 500;
    private static final String MSG_SUCCESS="success";
    private static final String MSG_FAIL="failed";

    /*错误码*/
    @ApiModelProperty("code 码：200 成功、500失败")
    private Integer code;

    /*提示信息 */
    @ApiModelProperty("提示消息")
    private String msg;

    /*具体内容*/
    @ApiModelProperty("业务数据")
    private  T data;

    public Result(){}

    public Result(int code) {
        this.code = code;
    }

    public Result(int code, T data) {
        this.code = code;
        this.data = data;
    }


    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static Result success() {
        return new Result(CODE_SUCCESS, MSG_SUCCESS);
    }

    public static Result success(Object data) {
        return new Result(CODE_SUCCESS, MSG_SUCCESS, data);
    }

    public static Result success(String msg, Object data) {
        return new Result(CODE_SUCCESS, msg, data);
    }

    public static Result fail() {
        return new Result(CODE_FAIL, MSG_FAIL);
    }

    public static Result fail(String msg) {
        return new Result(CODE_FAIL, msg);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}